const pkg = require("./package.json");
const webpack = require("webpack");

function flattenPkgName(name) {
  return name.replace("@", "").replace("/", "-");
}

module.exports = {
  configureWebpack: config => {
    if (process.env.NODE_ENV === "production") {
      config.output.filename = `js/${flattenPkgName(pkg.name)}.[name].js`;
      config.output.chunkFilename = `js/${flattenPkgName(pkg.name)}.[name].js`;
    }
    config.entry["editor.worker"] = "./node_modules/monaco-editor/esm/vs/editor/editor.worker.js";
    config.entry["json.worker"] = "./node_modules/monaco-editor/esm/vs/language/json/json.worker";
    config.entry["css.worker"] = "./node_modules/monaco-editor/esm/vs/language/css/css.worker";
    config.entry["html.worker"] = "./node_modules/monaco-editor/esm/vs/language/html/html.worker";
    config.entry["ts.worker"] = "./node_modules/monaco-editor/esm/vs/language/typescript/ts.worker";
  },
  chainWebpack: config => {
    if (process.env.NODE_ENV === "production") {
      config
        .plugin("extract-css")
          .tap(args => {
            args[0].filename = `css/${flattenPkgName(pkg.name)}.css`
            return args
          })
    }
    config
      .plugin("ignore-webpack")
        .use(webpack.IgnorePlugin, [
          /^((fs)|(path)|(os)|(crypto)|(source-map-support))$/,
          /vs\/language\/typescript\/lib/
        ])
  }
}
