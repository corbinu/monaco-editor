import { shallow } from "@vue/test-utils";

import Monaco from "../../src/Monaco.vue";

describe("Monaco.vue", () => {
    it("renders props.msg when passed", () => {
        const msg = "new message";
        const wrapper = shallow(Monaco, {
            "propsData": { msg }
        });

        expect(wrapper.text()).toMatch(msg);
    });
});
